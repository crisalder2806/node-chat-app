var socket = io();
socket.on('connect', function () {
    console.log('Connected to server');

    // socket.emit('createMessage', {
    //     from: 'giang@gmail.com',
    //     text: 'Hey. I love you'
    // })
});

socket.on('disconnect', function () {
    console.log('Unable to connect');
});

socket.on('newMessage', function (message) {
    var template = $('#msg-template').html();
    var html = Mustache.render(template, {
        text: message.text,
        from: message.from,
        createdAt: message.createdAt
    });

    $('#messages').append(html);
    // var li = jQuery('<li></li>');
    // li.text(`${message.from} ${message.createdAt}: ${message.text}`);

    // $('#messages').append(li);
});

socket.on('newLocation', function (location) {
    var template = $('#location-template').html();
    var html = Mustache.render(template, {
        from: 'Admin',
        url: location.url
    });
    // var li = jQuery('<li></li>');
    // var a = jQuery('<a>My current location</a>');
    // a.attr('href', location.url);
    // li.append(a);

    $('#messages').append(html);
})

$('#compose').on('submit', function (e) {
    e.preventDefault();
    socket.emit('createMessage', {
        from: 'User',
        text: $('[name="message"]').val()
    }, function (data) {
        $('[name="message"]').val("");
    });
});

var locationBtn = $('#locationBtn');
locationBtn.on('click', function () {
    if (!navigator.geolocation) {
        return alert('Geolocation is not supported');
    }

    navigator.geolocation.getCurrentPosition(function (position) {
        socket.emit('createLocationMessage', {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        });
    }, function (e) {
        alert('Cannot get location');
    });
});