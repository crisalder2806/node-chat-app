const path      = require('path');
const express   = require('express');
const socketIO  = require('socket.io');
const http      = require('http');
const _         = require('lodash');
var moment      = require('moment');

const publicPath    = path.join(__dirname, '../public');
const port          = process.env.PORT || 3000;
var app             = express();
var server          = http.createServer(app);
var io              = socketIO(server);

app.use(express.static(publicPath));

var generateLocationMsg = (from, lat, lng) => {
    return {
        from,
        text: 'text',
        url: 'https://www.google.com/maps?q=' + lat + ',' + lng,
        createdAt: generateCurrentDateTime()
    }
};

io.on('connection', (socket) => {
    console.log('New user Connected');

    // socket.emit from admin text welcome to chat app
    socket.broadcast.emit('newMessage', {
        from: 'Admin',
        text: 'New User Joined'
    })

    socket.emit('newMessage', {
        from: 'Admin',
        text: 'Welcome to the new world'
    })

    socket.on('createMessage', (newMessage, callback) => {
        var datetime = generateCurrentDateTime();
        newMessage.createdAt = datetime;
        io.emit('newMessage', newMessage);
        callback('This is from the server');
    })

    socket.on('createLocationMessage', (location) => {
        io.emit('newLocation', generateLocationMsg('Admin', location.lat, location.lng))
    })

    socket.on('disconnect', () => {
        console.log('User was disconnected');
    })
})

var generateCurrentDateTime = function () {
    var dt = new moment();
    return dt.format('h:mm a');
}

server.listen(port, () => {
    console.log('Server is up on port', port);
})